#!/bin/bash
# Copyright (C) 2016  Aleksandar Radivojevic <rzhw3h@gmail.com>

function from_dump
{
    dmidecode --from-dump $TEMP_DIR/dump -t $1
}

function main
{
    if [[ "$TEMP_DIR" == "" ]]; then
        echo -e "please run 'linfo-menu'"
        exit 1
    fi

    mkdir -p $TEMP_DIR
    if [[ ! -f $TEMP_DIR/dump ]] && [[ ! -f $TEMP_DIR/pci_dump ]]; then
        dmidecode --dump-bin $TEMP_DIR/dump &>/dev/null
        lspci > $TEMP_DIR/pci_dump
        menu
        rm -rf $TEMP_DIR
    else
        echo -e "dump exists!"
        exit 1
    fi
}

function menu
{
    get_gpu_vendor_logo
    logo "$GPU_VENDOR"
    bios
    motherboard
    cpu
    gpu
    ram
    sentinel
    read
}

function bios
{
    f=$(from_dump "bios" | grep "Vendor:\|Version:\|Release Date:\|UEFI" | tr -s [:space:])
    echo -e "BIOS:\e[33m $(regex "bios" "$f" 1) Version $(regex "bios" "$f" 2) [ $(regex "bios" "$f" 3) ]\e[0m"
}

function motherboard
{
    f=$(from_dump "baseboard" | grep "Manufacturer:\|Product Name:\|Version:" | tr -s [:space:])
    echo -e "Motherboard:\e[33m $(regex "mobo" "$f" 1) $(regex "mobo" "$f" 2) [ $(regex "mobo" "$f" 3) ]\e[0m"
}

function cpu
{
    f=$(from_dump "processor" | grep "Upgrade:\|Version:\|Core Count:" | tr -s [:space:])
    echo -e "CPU:\e[33m $(regex "cpu" "$f" 1) x$(regex "cpu" "$f" 3) [ $(regex "cpu" "$f" 2) ]\e[0m"
}

function ram
{
    f=$(from_dump "memory")
    len=$(regex "ram" "$f" 0)
    counter=0
    echo -e "RAM:\e[33m"
    for ((i = 0; i < $len; i=i+4)); do
        echo -e "    $(regex "ram" "$f" $(($i+4))) $(regex "ram" "$f" $(($i+5))) $(regex "ram" "$f" $(($i+2))) on $(regex "ram" "$f" $(($i+3)))"
        counter=$(($counter+1))
    done
    echo -e "\n    Total DIMMs: $counter"
    echo -e "    $(from_dump "memory" | grep "Maximum Capacity:" | xargs)\e[0m"
}

function gpu
{
    f=$(cat $TEMP_DIR/pci_dump | grep "VGA")
    echo -e "GPU:\e[33m $(regex "gpu" "$f" 0)\e[0m"
}

function get_gpu_vendor_logo
{
    f=$(cat $TEMP_DIR/pci_dump | grep "VGA")
    if [[ "$(echo "$f" | grep -i "AMD\|ATI")" ]]; then
        export GPU_VENDOR="amd"
    elif [[ "$(echo "$f" | grep -i "Intel")" ]]; then
        export GPU_VENDOR="intel"
    elif [[ "$(echo "$f" | grep -i "Nvidia")" ]]; then
        export GPU_VENDOR="nvidia"
    else
        export GPU_VENDOR="UNKNOWN"
    fi
}

function sentinel
{
    f=$("$R/HDSentinel" -solid)
    len=$(regex "sentinel" "$f" 0)
    counter=0
    echo -e "HDDSentinel:\e[33m"
    for ((i = 0; i < $len; i=i+4)); do
        echo -e "    $(regex "sentinel" "$f" $(($i+2))) $(regex "sentinel" "$f" $(($i+5))) $(regex "sentinel" "$f" $(($i+3)))°C HP:$(regex "sentinel" "$f" $(($i+4)))% $(($(regex "sentinel" "$f" $(($i+6)))/1024)) GiB"
        counter=$(($counter+1))
    done
}
